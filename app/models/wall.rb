require 'open-uri'

class Wall < ApplicationRecord
  URL = "https://vk.com/share.php?act=count&url="

  validates :url, presence: true
  validates :url, format: { with: URI.regexp }, if: 'url.present?'
  
  def get_shares
    str = open(URL + self.url).read
    self.shares = str.match(/\(\d, (.*?)\)/)[1]
  end
end
