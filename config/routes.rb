Rails.application.routes.draw do
  resources :walls

  root 'walls#new'

  get '/new', to: 'walls#new'
  post '/new',  to: 'walls#create'
end
