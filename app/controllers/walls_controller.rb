class WallsController < ApplicationController

  def show
    @wall = Wall.find(params[:id])
  end

  def new
    @wall = Wall.new
  end

  def create
    @wall = Wall.new(wall_params)
    (redirect_to @exst and return) if @exst = Wall.find_by(url: @wall.url)
    @wall.get_shares
    if @wall.save
      flash[:success] = "Сканирование выполнено!"
      redirect_to @wall
    else
      render 'new'
    end
  end

  private

  def wall_params
    params.require(:wall).permit(:shares, :url)
  end
end
