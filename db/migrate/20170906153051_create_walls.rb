class CreateWalls < ActiveRecord::Migration[5.1]
  def change
    create_table :walls do |t|
      t.integer :shares
      t.text :url

      t.timestamps
    end
  end
end
