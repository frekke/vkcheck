require 'test_helper'

class SiteTest < ActionDispatch::IntegrationTest
  test "layout links" do
    get root_path
    assert_template 'walls/new'
    assert_select "title", "Поиск лайков и репостов"
  end
  
  test "valid url" do
    get root_path
    assert_difference 'Wall.count', 1 do
      post walls_path, params: { wall: { url: "https://yandex.ru/", shares: 34} }
    end
    follow_redirect!
    assert_template 'walls/show'
    flash.empty?
    assert_select "title", "Результат"
  end

  test "invalid url" do
    get root_path
    assert_no_difference 'Wall.count' do
      post walls_path, params: { wall: { url: "123", shares: 34} }
    end
    assert_template 'walls/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end
end
